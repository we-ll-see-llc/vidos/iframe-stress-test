import React, {useEffect, useRef, useState} from 'react';
import './App.css';

interface Sample {
  numIframes: number;
  frameRate: number;
}

enum ActivityLevel {
  Silent,
  Quiet,
  Loud
}

const copyText = (str: string) => navigator.clipboard.writeText(str);

function App() {
  const [frameLengths, setFrameLengths] = useState<number[]>([]);

  const [numIframes, setNumIframes] = useState(0);

  const [samples, setSamples] = useState<Sample[]>([]);

  const [testRunning, setTestRunning] = useState(false);

  const [testComplete, setTestComplete] = useState(false);

  const urlParams = new URLSearchParams(window.location.search);
  const [activityLevel, setActivityLevel] = useState<ActivityLevel>(() => {
   const param = urlParams.get('activity_level');

   if (!param) {
     return ActivityLevel.Loud;
   }

   return parseInt(param);
  });

  // in parent pings = received, in child pings = sent.
  const [pings, setPings] = useState(0);

  useEffect(() => {
    if (window.top !== window.self && activityLevel === ActivityLevel.Silent) {
      return;
    }

    if (window.top !== window.self && activityLevel === ActivityLevel.Quiet) {
      const delayAndPing = () => {
        setTimeout(() => {
          setPings((s) => s + 1);
          delayAndPing();
        }, Math.random() * 2000)
      };

      delayAndPing();
    }

    if (window.top === window.self || activityLevel === ActivityLevel.Loud) {
      let last = new Date();

      const f = () => {
        const now = new Date();

        const frameLength = now.getTime() - last.getTime();

        setFrameLengths(fls => {
          const newFls = [...fls];

          while (newFls.length > 60) {
            newFls.shift();
          }

          newFls.push(frameLength);

          return newFls;
        })

        last = now;

        requestAnimationFrame(f);
      }

      requestAnimationFrame(f);
    }

  // eslint-disable-next-line
  }, []);

  const frameRate = Math.round(100000 / (frameLengths.reduce((a, b) => a + b, 0) / (frameLengths.length))) / 100;

  const frameRateRef = useRef(frameRate);
  frameRateRef.current = frameRate;

  useEffect(() => {
    if (!testRunning) {
      return;
    }

    if (window.top === window.self) {
      const clear = setTimeout(() => {
        setNumIframes((n => n + 1));

        setSamples((s) => {
          return [...s, {
            frameRate: frameRateRef.current,
            numIframes: numIframes,
          }];
        });

        if (numIframes === 151) {
          setTestComplete(true);
          setTestRunning(false);
        }
      }, 3000);

      return () => clearTimeout(clear);
    }

  // eslint-disable-next-line
  }, [numIframes, testRunning]);

  const startTest = (): void => {
    setTestRunning(true);
    setNumIframes(0);
    setSamples([]);
  }

  const copyData = () => {
    let testData = `activityLevel=${ActivityLevel[activityLevel]}
hardwareConcurrency=${navigator.hardwareConcurrency}
    
# of iframes,frame rate    
`;

    samples.forEach((s) => {
      testData += `${s.numIframes},${s.frameRate}\n`;
    });

    copyText(testData).catch(console.log).then(() => console.log(testData));
  }

  const showFrameRate = window.self === window.parent || activityLevel === ActivityLevel.Loud;

  const testRunningView = () => <>
    {showFrameRate ?
        <p>
          Frame Rate
          <br/>
          {frameRate}
        </p>
        : <p>pings: {pings}</p>
    }

    activity level:
    <select onChange={(e) => {
      console.log(e.target.value);
      setActivityLevel(parseInt(e.target.value))
    }} value={activityLevel}>
      <option value={ActivityLevel.Silent}>{ActivityLevel[ActivityLevel.Silent]}</option>
      <option value={ActivityLevel.Quiet}>{ActivityLevel[ActivityLevel.Quiet]}</option>
      <option value={ActivityLevel.Loud}>{ActivityLevel[ActivityLevel.Loud]}</option>
    </select>

    <button onClick={startTest}>
      Start test
    </button>

    <div className="frameContainer">
      {new Array(numIframes).fill(0).map((_, idx) => {
        return <iframe className="iframe" key={idx} title={`iframe ${idx}`} src={`${process.env.PUBLIC_URL}/index.html?activity_level=${activityLevel}`}/>;
      })
      }
    </div>
  </>;

  const testCompleteView = () => <>
    Test is complete! Click below to copy the test data. Please send it to the test administrator.

    <button onClick={copyData}>copy data</button>
  </>;

  return (
    <div className="App">
      <header className="App-header">
        { !testComplete && testRunningView()}
        { testComplete && testCompleteView()}
      </header>
    </div>
  );
}

export default App;
